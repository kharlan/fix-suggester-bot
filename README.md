# Fix Suggester Bot

Bot for processing events on code review systems (Gerrit now, GitLab later) and posting fix suggestions from code 
quality tools (phpcs now, others later).

## Toolforge implementation

Please see [Tool:Fix_Suggester_Bot](https://wikitech.wikimedia.org/wiki/Tool:Fix_Suggester_Bot)

## Local development

To test the end-to-end workflow, create a `.env.local` file:

```dotenv
GERRIT_USERNAME=(your username)
GERRIT_PORT=29418
GERRIT_HOST=gerrit.wikimedia.org
GERRIT_URL=https://gerrit.wikimedia.org
GERRIT_PASSWORD=(your http password)
GERRIT_ROBOT_URL="https://gerrit.wikimedia.org"
GERRIT_ROBOT_ID=fixsuggesterbot
CACHE_DIRECTORY=/tmp/fix-suggester-bot/cache
OUTPUT_DIRECTORY=/tmp/fix-suggester-bot/reports
GERRIT_PROJECT_SAFELIST=mediawiki/extensions/GrowthExperiments
GERRIT_PROJECT_SAFELIST_PREFIXES=mediawiki
GERRIT_SSH_PRIVATE_KEY_PATH=(not needed for local dev)
DATABASE_URL="mysql://root:root@localhost/fixsuggesterbot"
```

You'll need a JSON stream entry from Gerrit. You can use a predefined one in `tests/fixtures/growthexperiments.json`

Then run `bin/console fix-suggester-bot:subscribe:gerrit tests/fixtures/growthexperiments.json`. This adds the patch metadata to the message queue.

You can then run `bin/console messenger:consume gerrit_async -vv` to consume the message. (If you run this via a PHP script in PHPStorm, you can use XDebug.)
