<?php

namespace App\Tests\Unit\Message;

use App\Message\GerritStream;
use PHPUnit\Framework\TestCase;

#[Covers('\App\Message\GerritStream')]
class GerritStreamTest extends TestCase {

	public function testConstruct() {
		$this->assertInstanceOf( GerritStream::class, new GerritStream( '' ) );
	}

	public function testGetContent() {
		$message = new GerritStream( 'Foo' );
		$this->assertEquals( 'Foo', $message->getContent() );
	}
}
