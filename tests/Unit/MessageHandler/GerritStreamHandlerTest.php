<?php

namespace App\Test\Unit\MessageHandler;

use App\Message\GerritStream;
use App\MessageHandler\GerritStreamHandler;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

#[Covers('\App\MessageHandler\GerritStreamHandler')]
class GerritStreamHandlerTest extends TestCase {

	public function testProjectSafelist() {
		$gerritStreamHandler = new GerritStreamHandler(
			'',
			'',
			'foo',
			'bar',
			'baz',
			'bam',
			'https://test',
			'Foo|Bar',
			''
		);
		$gerritStreamHandler->setLogger( new NullLogger() );
		$gerritStream = new GerritStream( json_encode( [
			'patchSet' => [ 'ref' => 1 ],
			'change' => [
				'number' => 2,
				'project' => 'Baz',
			],
		] ) );
		$this->assertNull( $gerritStreamHandler( $gerritStream ) );
	}

	public function testProjectPrefixSafelist() {
		$gerritStreamHandler = new GerritStreamHandler(
			sys_get_temp_dir() . '/fix-suggester-bot-tests/cache',
			sys_get_temp_dir() . '/fix-suggester-bot-tests/cache',
			'foo',
			'bar',
			'baz',
			'bam',
			'https://test',
			null,
			'mediawiki/extensions'
		);
		$gerritStreamHandler->setLogger( new NullLogger() );

		$gerritStream = new GerritStream( json_encode( [
			'project' => 'wikimedia/extensions/Bar',
			'patchSet' => [ 'ref' => 1 ],
			'change' => [
				'project' => 'wikimedia/extensions/Bar',
				'number' => 2
			],
		] ) );

		$this->assertNull( $gerritStreamHandler( $gerritStream ) );
	}
}
