<?php

namespace App\Message;

class GerritStream {

	/** @var string Raw Gerrit stream item content. */
	private $content;

	/**
	 * @param string $content
	 */
	public function __construct( string $content ) {
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getContent(): string {
		return $this->content;
	}
}
