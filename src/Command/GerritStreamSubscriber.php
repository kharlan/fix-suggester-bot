<?php

namespace App\Command;

use App\Message\GerritStream;
use phpseclib3\Crypt\PublicKeyLoader;
use phpseclib3\Net\SSH2;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class GerritStreamSubscriber extends Command {

	/**
	 * @var MessageBusInterface
	 */
	private $messageBus;

	/** @var string */
	private $gerritUsername;

	/** @var string */
	private $gerritHost;

	/** @var string */
	private $gerritPort;
	/**
	 * @var string
	 */
	private $gerritSshPrivateKeyPath;

	/**
	 * @param MessageBusInterface $messageBus
	 * @param string $gerritUsername
	 * @param string $gerritPort
	 * @param string $gerritHost
	 * @param string $gerritSshPrivateKeyPath
	 */
	public function __construct(
		MessageBusInterface $messageBus,
		string $gerritUsername,
		string $gerritPort,
		string $gerritHost,
		string $gerritSshPrivateKeyPath
	) {
		$this->messageBus = $messageBus;
		$this->gerritUsername = $gerritUsername;
		$this->gerritPort = $gerritPort;
		$this->gerritHost = $gerritHost;
		$this->gerritSshPrivateKeyPath = $gerritSshPrivateKeyPath;
		parent::__construct();
	}

	/** @inheritDoc */
	public function configure() {
		$this->setName( 'fix-suggester-bot:subscribe:gerrit' );
		$this->setDescription( 'Subscribe to gerrit stream events, and generate/post fix suggestions.' )
			->setHelp( 'Command to subscribe to stream of events from Gerrit server. Suggestions are generated ' .
				' and submitted as fix suggestions in Gerrit' );
		$this->addArgument(
			'json',
			InputArgument::OPTIONAL,
			'Path to a file with the raw JSON of an event, useful for debugging'
		);
		$this->addOption(
			'host',
			null,
			InputOption::VALUE_REQUIRED,
			'The gerrit host to connect to',
			$this->gerritHost ?: 'gerrit.wikimedia.org'
		);
		$this->addOption(
			'port',
			null,
			InputOption::VALUE_REQUIRED,
			'The port to connect to',
			$this->gerritPort ?: 29418
		);
		$this->addOption(
			'username',
			null,
			InputOption::VALUE_REQUIRED,
			'The username to use in SSH connection to gerrit host',
			$this->gerritUsername
		);
		$this->addOption(
			'ssh-private-key-path',
			null,
			InputOption::VALUE_REQUIRED,
			'The path to the SSH private key to use in authentication',
			$this->gerritSshPrivateKeyPath
		);
	}

	/** @inheritDoc */
	public function execute( InputInterface $input, OutputInterface $output ): int {
		$json = $input->getArgument( 'json' );
		if ( $json ) {
			$this->messageBus->dispatch( new GerritStream( file_get_contents( $json ) ) );
			return Command::SUCCESS;
		}
		$host = $input->getOption( 'host' );
		$ssh = new SSH2( $host, (int)$input->getOption( 'port' ) );
		$key = PublicKeyLoader::load( file_get_contents( $this->gerritSshPrivateKeyPath ) );
		if ( !$ssh->login( $input->getOption( 'username' ), $key ) ) {
			throw new \Exception( 'Login failed' );
		}
		// Generously assume that a patchset is created at least once per 24 hour interval.
		$ssh->setKeepAlive( 86400 );
		$ssh->setTimeout( 86400 );
		$output->writeln( "Successfully connected to $host" );
		$ssh->exec( 'gerrit stream-events --subscribe patchset-created', function ( $streamItem ) use ( $output ) {
			$this->messageBus->dispatch( new GerritStream( $streamItem ) );
			$output->writeln( $streamItem );
		} );

		return Command::SUCCESS;
	}

}
