<?php

namespace App\MessageHandler;

use App\Generator\Gerrit;
use App\Message\GerritStream;
use PharData;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Process\Process;

class GerritStreamHandler implements MessageHandlerInterface, LoggerAwareInterface {

	/**
	 * @var string
	 */
	private $cacheDirectory;
	/**
	 * @var string
	 */
	private $outputDirectory;
	/**
	 * @var string
	 */
	private $gerritRobotId;
	/**
	 * @var string
	 */
	private $gerritRobotUrl;
	/**
	 * @var string
	 */
	private $gerritUsername;
	/**
	 * @var string
	 */
	private $gerritPassword;
	/**
	 * @var string
	 */
	private $gerritUrl;
	/**
	 * @var array
	 */
	private $gerritProjectSafelist;
	/**
	 * @var LoggerInterface
	 */
	private $logger;
	/**
	 * @var string[]
	 */
	private $gerritProjectSafelistPrefixes;

	/**
	 * @param string $cacheDirectory
	 * @param string $outputDirectory
	 * @param string $gerritRobotId
	 * @param string $gerritRobotUrl
	 * @param string $gerritUsername
	 * @param string $gerritPassword
	 * @param string $gerritUrl
	 * @param string|null $gerritProjectSafelist
	 * @param string|null $gerritProjectSafelistPrefixes
	 */
	public function __construct(
		string $cacheDirectory,
		string $outputDirectory,
		string $gerritRobotId,
		string $gerritRobotUrl,
		string $gerritUsername,
		string $gerritPassword,
		string $gerritUrl,
		?string $gerritProjectSafelist,
		?string $gerritProjectSafelistPrefixes
	) {
		$this->cacheDirectory = $cacheDirectory;
		$this->outputDirectory = $outputDirectory;
		$this->gerritRobotId = $gerritRobotId;
		$this->gerritRobotUrl = $gerritRobotUrl;
		$this->gerritUsername = $gerritUsername;
		$this->gerritPassword = $gerritPassword;
		$this->gerritUrl = $gerritUrl;
		$this->gerritProjectSafelist = array_filter( explode( '|', $gerritProjectSafelist ?? '' ) );
		$this->gerritProjectSafelistPrefixes = array_filter(
			explode( '|', $gerritProjectSafelistPrefixes ??	'' )
		);
	}

	/**
	 * @param GerritStream $gerritStream
	 */
	public function __invoke( GerritStream $gerritStream ) {
		$streamContent = json_decode( $gerritStream->getContent(), true );
		if ( !$streamContent ) {
			throw new UnrecoverableMessageHandlingException(
				"Unable to decode stream item: " . $gerritStream->getContent()
			);
		}
		// e.g. refs/changes/18/713818/4
		$ref = $streamContent['patchSet']['ref'];
		$project = $streamContent['change']['project'];
		if ( count( $this->gerritProjectSafelist ) && !in_array( $project, $this->gerritProjectSafelist ) ) {
			return;
		}
		if ( count( $this->gerritProjectSafelistPrefixes ) ) {
			$prefixFound = false;
			foreach ( $this->gerritProjectSafelistPrefixes as $prefix ) {
				if ( strpos( $project, $prefix ) === 0 ) {
					$prefixFound = true;
					break;
				}
			}
			if ( !$prefixFound ) {
				$this->logger->info( "$project does not contain a prefix set in GERRIT_PROJECT_SAFELIST_PREFIXES" );
				return;
			}
		}
		$zuulChange = $streamContent['change']['number'];
		$zuulPatchsetNumber = $streamContent['patchSet']['number'];
		$robotRunId = sprintf( '%d-%d', $zuulChange, $zuulPatchsetNumber );
		$gerritCacheDirectory = sprintf( '%s/%s', $this->cacheDirectory, 'gerrit' );
		if ( !file_exists( $gerritCacheDirectory ) ) {
			$this->logger->info( "Creating $gerritCacheDirectory " );
			mkdir( $gerritCacheDirectory, 0755, true );
		}

		$projectNameUnderscores = str_replace( '/', '-', $project );
		$uniqueProjectName = sprintf( "%s-%d-%d", $projectNameUnderscores, $zuulChange,
			$zuulPatchsetNumber );

		$downloadUrl = sprintf(
			"%s/r/changes/%s~%d/revisions/%d/archive?format=tgz",
			$this->gerritUrl,
			urlencode( $project ),
			$zuulChange,
			$zuulPatchsetNumber
		);
		$downloadFilename = sprintf( "%s/%s.tgz", sys_get_temp_dir(), $uniqueProjectName );
		$this->logger->info(
			"Getting files for $uniqueProjectName in $gerritCacheDirectory with $downloadUrl to $downloadFilename"
		);
		if ( file_exists( $downloadFilename ) ) {
			unlink( $downloadFilename );
		}
		file_put_contents(
			$downloadFilename,
			file_get_contents( $downloadUrl )
		);
		$workingDirectory = sprintf( '%s/%s', $gerritCacheDirectory, $uniqueProjectName );
		if ( file_exists( $workingDirectory ) ) {
			$this->logger->info( "Removing $workingDirectory" );
			$filesytem = new Filesystem();
			$filesytem->remove( $workingDirectory );
		}
		$phar = new PharData( $downloadFilename );
		$phar->extractTo( $workingDirectory );
		unlink( $downloadFilename );

		$this->logger->info( "Creating composer.json with project-specific mediawiki-codesniffer" );
		$composerJson = json_decode( file_get_contents( sprintf( "%s/composer.json", $workingDirectory ) ), true );
		if ( !$composerJson ) {
			return;
		}
		$version = $composerJson['require-dev']['mediawiki/mediawiki-codesniffer'];
		unlink( sprintf( "%s/composer.json", $workingDirectory ) );
		if ( file_exists( sprintf( "%s/composer.lock", $workingDirectory ) ) ) {
			unlink( sprintf( "%s/composer.lock", $workingDirectory ) );
		}
		$this->logger->info( "Creating new composer.json file with mediawiki-codesniffer=$version" );
		$process = new Process( [
			'composer',
			  'init',
			  '-n',
			  "--require-dev=mediawiki/mediawiki-codesniffer=$version",
			  "--require=sebastian/diff=~3"
		], $workingDirectory );
		$process->run();
		$this->logger->info( $process->getOutput() );

		// Allow plugins
		$process = new Process( [
			'composer',
			'config',
			'allow-plugins',
			'true'
		], $workingDirectory );
		$process->run();
		$this->logger->info( $process->getOutput() );

		$this->logger->info( "Running composer install" );
		$process = new Process(
			[ 'composer', 'install', '-n', '--prefer-dist' ],
			$workingDirectory
		);
		$process->run();

		// Get files
		$this->logger->info( 'Getting list of files to run phpcs with' );
		$downloadUrl = sprintf(
			"%s/r/changes/%s~%d/revisions/%d/files",
			$this->gerritUrl,
			urlencode( $project ),
			$zuulChange,
			$zuulPatchsetNumber
		);
		$files = str_replace( ")]}'\n", '', file_get_contents( $downloadUrl ) );
		$filesJson = json_decode( $files, true );
		$filesToCheck = array_filter( $filesJson, static function ( $name ) {
			return $name !== '/COMMIT_MSG';
		}, ARRAY_FILTER_USE_KEY );

		$this->logger->info( "Generating fix suggestions" );
		$gerritGenerator = new Gerrit(
			sprintf( '%s/%s.json', $this->cacheDirectory, $projectNameUnderscores ),
			array_keys( $filesToCheck ),
			$workingDirectory,
			$this->gerritRobotId,
			$robotRunId,
			$this->gerritRobotUrl,
			$this->outputDirectory,
			$project
		);
		$gerritGenerator->setTools( [ 'phpcs' ] );
		$result = $gerritGenerator->execute();
		if ( !$result ) {
			$this->logger->info( 'No suggestions generated.' );
			$this->cleanup( $workingDirectory );
			return;
		}
		$this->logger->info( json_encode( $result, JSON_PRETTY_PRINT ) );
		foreach ( $result['suggestions'] as $tool => $fixFile ) {
			$this->logger->info( "Submitting suggestion for $tool from $fixFile" );
			$suggestions = json_decode( file_get_contents( $fixFile ), true );
			if ( !$suggestions ) {
				$this->logger->error( "Unable to load suggestion from $fixFile" );
				continue;
			}
			$this->logger->info( "Removing $fixFile" );
			unlink( $fixFile );
			$gerritSubmitter = new \App\Submitter\Gerrit(
				$this->gerritUrl,
				$this->gerritUsername,
				$this->gerritPassword,
				$project,
				$zuulChange,
				$zuulPatchsetNumber
			);
			$response = $gerritSubmitter->execute( $suggestions );
			$this->logger->info( $response->getContent( false ) );
		}
		$this->cleanup( $workingDirectory );
		$this->logger->info( "Finished processing $zuulChange-$zuulPatchsetNumber" );
	}

	/**
	 * Remove the working directory.
	 *
	 * @param string $workingDirectory
	 * @return void
	 */
	private function cleanup( string $workingDirectory ): void {
		$this->logger->info( "Removing $workingDirectory" );
		$process = new Process( [ 'rm', '-rf', $workingDirectory ] );
		$process->run();
	}

	/**
	 * @param LoggerInterface $logger
	 */
	public function setLogger( LoggerInterface $logger ) {
		$this->logger = $logger;
	}
}
